package com.xpressdocs.Controllers;




import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

import com.facebook.ads.sdk.AdImage;
import com.xpressdocs.Model.AdImageModel;
import com.xpressdocs.Service.AdImageService;

@RestController
@RequestMapping("/adimage")
public class AdImageController {
	@Autowired
	AdImageService ais;

//	@RequestMapping(value = "/upload", method = RequestMethod.POST)
//	public String uploadImage(@RequestParam("uploadedFile") MultipartFile file) {
//		return ais.uploadImage(file).toString();
//
//	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String uploadImage(@RequestBody AdImageModel uploadFile) {
		
		
		
		return ais.uploadImage(uploadFile.getUploadFile());

	}

}
