package com.xpressdocs.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import org.springframework.stereotype.Service;
import com.facebook.ads.sdk.AdSet;
import com.facebook.ads.sdk.CustomAudience;
import com.facebook.ads.sdk.CustomAudience.EnumSubtype;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.xpressdocs.Model.AdSetModel;
import com.xpressdocs.Utils.FacebookConfig;

@Service
public class AdSetServiceImpl extends FacebookConfig implements AdSetService {

	@Override
	public String createAdSet(AdSetModel adSetObj) {

		buildConfig();
		try {

			AdSet adset = getAccount().createAdSet().setName(adSetObj.getAdSetName())
					.setCampaignId(adSetObj.getCampaignId()).setStatus(adSetObj.getAdSetStatus())
					.setBillingEvent(adSetObj.getAdBillingEvent()).setDailyBudget(adSetObj.getAdDailyBudget())
					/*.setBidAmount(adSetObj.getAdBidAmount())/*.setOptimizationGoal(adSetObj.getAdOptimizationGoal())*/
					.setTargeting(adSetObj.getAdTargeting()) //.setFieldCustomAudiences("[{id:" + audience.getId() + "}]")
					.setRedownload(true).setIsAutobid(true)./*setPromotedObject("{'page_id': 260375097778725,}").*/execute();
			return adset.toString();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return e.getMessage();
		}

	}

	public static String sha256(String message) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
			return toHex(hash);
		} catch (Exception e) {
			return null;
		}
	}

	public CustomAudience getAudience(){
		CustomAudience audience = null;
		JsonArray schema = new JsonArray();
	      schema.add(new JsonPrimitive("EMAIL_SHA256"));
	      
	      JsonArray personA = new JsonArray();
	      personA.add(new JsonPrimitive(sha256("rforrizwan55@yahoo.com")));
	      JsonArray data = new JsonArray();
	      data.add(personA);
	      
	      JsonObject payload = new JsonObject();
	      payload.add("schema", schema);
	      payload.add("data", data);
	      
	      try{
	      
		  audience = getAccount().createCustomAudience()
			        .setName("Java SDK Test Custom Audience")
			        .setDescription("Test Audience")
			        .setSubtype(EnumSubtype.VALUE_CUSTOM)
			        .execute();
		
	      
	      audience.createUser()
	        .setPayload(payload.toString())
	        .execute();
	      }catch (Exception e) {
			// TODO: handle exception
		}
	      
	      return audience;
	}

	public static String toHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : bytes) {
			sb.append(String.format("%1$02x", b));
		}
		return sb.toString();
	}

}
