package com.xpressdocs.Service;

import com.facebook.ads.sdk.AdCreative;
import com.xpressdocs.Model.AdCreativeModel;

public interface AdCreativeService {
	
	public String createAdCreative(AdCreativeModel acmObj);

}
