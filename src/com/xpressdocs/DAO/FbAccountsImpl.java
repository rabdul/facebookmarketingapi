package com.xpressdocs.DAO;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xpressdocs.Model.FacebookAccount;


@Repository
public class FbAccountsImpl  implements FbAccounts {

	@Autowired
	private SessionFactory session;
	
	@Transactional
	@Override
	public FacebookAccount getFacebookAccountById(int accountId) {
		
		return (FacebookAccount)session.getCurrentSession().get(FacebookAccount.class, accountId);
	}

}
