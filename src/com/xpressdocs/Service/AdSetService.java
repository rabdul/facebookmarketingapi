package com.xpressdocs.Service;

import com.facebook.ads.sdk.AdSet;
import com.xpressdocs.Model.AdSetModel;


public interface AdSetService {
	public String createAdSet(AdSetModel obj);
}
