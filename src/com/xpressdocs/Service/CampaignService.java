package com.xpressdocs.Service;

import com.facebook.ads.sdk.Campaign;
import com.xpressdocs.Model.CampaignModel;

public interface CampaignService {
	
	public Campaign createCampaign(CampaignModel obj);

}
