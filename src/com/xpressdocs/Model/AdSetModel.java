package com.xpressdocs.Model;

import java.util.Arrays;
import com.facebook.ads.sdk.AdSet;
import com.facebook.ads.sdk.Targeting;
import com.facebook.ads.sdk.TargetingGeoLocation;

public class AdSetModel {

	private String adSetName;
	private String campaignId;
	private String adSetStatus;
	private String adBillingEvent;
	private String adDailyBudget;
	private String adBidAmount;
	private String adOptimizationGoal;
	private String adTargeting;
	private Boolean adRedownload;

	public String getAdSetName() {
		return adSetName;
	}

	public void setAdSetName(String adSetName) {
		this.adSetName = adSetName;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public AdSet.EnumStatus getAdSetStatus() {
		return getAdSetStatus(adSetStatus);
	}

	public void setAdSetStatus(String adSetStatus) {
		this.adSetStatus = adSetStatus;
	}

	public AdSet.EnumBillingEvent getAdBillingEvent() {
		return getAdBillingEvent(adBillingEvent);
	}

	public void setAdBillingEvent(String adBillingEvent) {
		this.adBillingEvent = adBillingEvent;
	}

	public Long getAdDailyBudget() {
		return Long.parseLong(adDailyBudget);
	}

	public void setAdDailyBudget(String adDailyBudget) {
		this.adDailyBudget = adDailyBudget;
	}

	public Long getAdBidAmount() {
		return Long.parseLong(adBidAmount);
	}

	public void setAdBidAmount(String adBidAmount) {
		this.adBidAmount = adBidAmount;
	}

	public AdSet.EnumOptimizationGoal getAdOptimizationGoal() {
		return getAdOptimizationGoal(adOptimizationGoal);
	}

	public void setAdOptimizationGoal(String adOptimizationGoal) {
		this.adOptimizationGoal = adOptimizationGoal;
	}

	public Targeting getAdTargeting() {
		return getAdTargeting(adTargeting);
	}

	public void setAdTargeting(String adTargeting) {
		this.adTargeting = adTargeting;
	}

	public Boolean getAdRedownload() {
		return adRedownload;
	}

	public void setAdRedownload(Boolean adRedownload) {
		this.adRedownload = adRedownload;
	}

	private AdSet.EnumStatus getAdSetStatus(String adSetStatus) {

		switch (adSetStatus.toLowerCase()) {
		case "null":
			return AdSet.EnumStatus.NULL;
		case "active":
			return AdSet.EnumStatus.VALUE_ACTIVE;
		case "archived":
			return AdSet.EnumStatus.VALUE_ARCHIVED;
		case "deleted":
			return AdSet.EnumStatus.VALUE_DELETED;
		case "paused":
			return AdSet.EnumStatus.VALUE_PAUSED;

		default:
			return AdSet.EnumStatus.NULL;
		}

	}

	private AdSet.EnumBillingEvent getAdBillingEvent(String adBillingEvent) {

		switch (adBillingEvent.toLowerCase()) {
		case "null":
			return AdSet.EnumBillingEvent.NULL;
		case "app_installs":
			return AdSet.EnumBillingEvent.VALUE_APP_INSTALLS;
		case "clicks":
			return AdSet.EnumBillingEvent.VALUE_CLICKS;
		case "impressions":
			return AdSet.EnumBillingEvent.VALUE_IMPRESSIONS;
		case "link_clicks":
			return AdSet.EnumBillingEvent.VALUE_LINK_CLICKS;
		case "mrc_video_views":
			return AdSet.EnumBillingEvent.VALUE_MRC_VIDEO_VIEWS;
		case "offer_claims":
			return AdSet.EnumBillingEvent.VALUE_OFFER_CLAIMS;
		case "page_likes":
			return AdSet.EnumBillingEvent.VALUE_PAGE_LIKES;
		case "post_engagement":
			return AdSet.EnumBillingEvent.VALUE_POST_ENGAGEMENT;
		case "video_views":
			return AdSet.EnumBillingEvent.VALUE_VIDEO_VIEWS;

		default:
			return AdSet.EnumBillingEvent.NULL;
		}

	}

	private AdSet.EnumOptimizationGoal getAdOptimizationGoal(String getAdOptimizationGoal) {

		switch (getAdOptimizationGoal.toLowerCase()) {
		case "null":
			return AdSet.EnumOptimizationGoal.NULL;
		case "app_downloads":
			return AdSet.EnumOptimizationGoal.VALUE_APP_DOWNLOADS;
		case "app_installs":
			return AdSet.EnumOptimizationGoal.VALUE_APP_INSTALLS;
		case "brand_awareness":
			return AdSet.EnumOptimizationGoal.VALUE_BRAND_AWARENESS;
		case "clicks":
			return AdSet.EnumOptimizationGoal.VALUE_CLICKS;
		case "engaged_users":
			return AdSet.EnumOptimizationGoal.VALUE_ENGAGED_USERS;
		case "event_responses":
			return AdSet.EnumOptimizationGoal.VALUE_EVENT_RESPONSES;
		case "impressions":
			return AdSet.EnumOptimizationGoal.VALUE_IMPRESSIONS;
		case "lead_generations":
			return AdSet.EnumOptimizationGoal.VALUE_LEAD_GENERATION;
		case "link_clicks":
			return AdSet.EnumOptimizationGoal.VALUE_LINK_CLICKS;
		case "none":
			return AdSet.EnumOptimizationGoal.VALUE_NONE;
		case "offer_claims":
			return AdSet.EnumOptimizationGoal.VALUE_OFFER_CLAIMS;
		case "offsite_conversions":
			return AdSet.EnumOptimizationGoal.VALUE_OFFSITE_CONVERSIONS;
		case "page_engagement":
			return AdSet.EnumOptimizationGoal.VALUE_PAGE_ENGAGEMENT;
		case "page_likes":
			return AdSet.EnumOptimizationGoal.VALUE_PAGE_LIKES;
		case "post_engagement":
			return AdSet.EnumOptimizationGoal.VALUE_POST_ENGAGEMENT;
		case "reach":
			return AdSet.EnumOptimizationGoal.VALUE_REACH;
		case "social_impressions":
			return AdSet.EnumOptimizationGoal.VALUE_SOCIAL_IMPRESSIONS;
		case "video_views":
			return AdSet.EnumOptimizationGoal.VALUE_VIDEO_VIEWS;
		default:
			return AdSet.EnumOptimizationGoal.NULL;
		}
	}
	
	private Targeting getAdTargeting(String adTargeting){
		 Targeting test = new Targeting().setFieldGeoLocations(new TargetingGeoLocation().setFieldCountries(Arrays.asList("US")));
		return test;
	}

}
