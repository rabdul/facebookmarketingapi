package com.xpressdocs.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xpressdocs.Model.AdModel;
import com.xpressdocs.Service.AdService;

@RestController
@RequestMapping("/ad")
public class AdController {

	@Autowired
	AdService as;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createAd(@RequestBody AdModel am) {
		return as.createAd(am);
	}
}
