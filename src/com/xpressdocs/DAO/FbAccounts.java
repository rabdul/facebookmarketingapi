package com.xpressdocs.DAO;

import com.xpressdocs.Model.FacebookAccount;

public interface FbAccounts {
	
	public FacebookAccount getFacebookAccountById(int accountId);

}
