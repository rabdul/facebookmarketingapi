package com.xpressdocs.Utils;

import org.springframework.beans.factory.annotation.Autowired;

import com.facebook.ads.sdk.APIContext;
import com.facebook.ads.sdk.AdAccount;
import com.xpressdocs.DAO.FbAccounts;
import com.xpressdocs.Model.FacebookAccount;

public class FacebookConfig {
	@Autowired
	FbAccounts fbAccount;
	FacebookAccount fbA;
	APIContext context;
	AdAccount account;

	public FbAccounts getFbAccount() {
		return fbAccount;
	}

	public void setFbAccount(FbAccounts fbAccount) {
		this.fbAccount = fbAccount;
	}

	public FacebookAccount getFbA() {
		return fbA;
	}

	public void setFbA(FacebookAccount fbA) {
		this.fbA = fbA;
	}

	public APIContext getContext() {
		return context;
	}

	public void setContext(APIContext context) {
		this.context = context;
	}

	public AdAccount getAccount() {
		return account;
	}

	public void setAccount(AdAccount account) {
		this.account = account;
	}

	public void buildConfig() {
		fbA = fbAccount.getFacebookAccountById(1);
		context = new APIContext(fbA.getAccessToken(), fbA.getAppSecert()).enableDebug(true).setLogger(System.out);
		account = new AdAccount(fbA.getAccountID(), context);
	}

}
