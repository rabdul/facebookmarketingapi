package com.xpressdocs.Service;

import org.springframework.stereotype.Service;

import com.facebook.ads.sdk.APIException;
import com.facebook.ads.sdk.Ad;
import com.facebook.ads.sdk.AdCreative;
import com.xpressdocs.Model.AdModel;
import com.xpressdocs.Utils.FacebookConfig;

@Service
public class AdServiceImpl extends FacebookConfig implements AdService {

	@Override
	public String createAd(AdModel amObj) {
		buildConfig();
		 Ad ad = null;
		 try{
		  ad = getAccount().createAd()
			        .setName(amObj.getAdName())
			        .setAdsetId(Long.parseLong(amObj.getAdSetId()))
			        .setCreative(getAdCreativeById(amObj.getAdCreativeId()))
			        .setStatus(amObj.getAdStatus())
			        .setBidAmount(amObj.getAdBidAmount())
			        .setRedownload(amObj.getAdReDownload())
			        .execute();
		  
		  return ad.toString();
		 }
		 catch (Exception e) {
			 System.err.println(e.getMessage());
			 return e.getMessage();
		}
		
	}
	
	private AdCreative getAdCreativeById(String adCreativeId) {
		try {
			return AdCreative.fetchById(adCreativeId, getContext());
		} catch (APIException e) {
			e.printStackTrace();
			return null;
		}
	}

}
