package com.xpressdocs.Service;

import org.springframework.stereotype.Service;

import com.facebook.ads.sdk.AdCreative;
import com.facebook.ads.sdk.AdCreativeLinkData;
import com.facebook.ads.sdk.AdCreativeObjectStorySpec;
import com.facebook.ads.sdk.AdPreview.EnumAdFormat;
import com.xpressdocs.Model.AdCreativeModel;
import com.xpressdocs.Utils.FacebookConfig;

@Service
public class AdCreativeServiceImpl extends FacebookConfig implements AdCreativeService {

	@Override
	public String createAdCreative(AdCreativeModel acmObj) {

		buildConfig();
		AdCreative creative = null;
		try {
			creative = getAccount().createAdCreative().setTitle(acmObj.getAdCreativeName())
					.setBody(acmObj.getAdCreativeBody()).setImageHash(acmObj.getAdImageHash())
					.setLinkUrl(acmObj.getAdCreativeLinkUrl()).setObjectStorySpec(new AdCreativeObjectStorySpec().setFieldPageId("260375097778725").setFieldLinkData(new AdCreativeLinkData().setFieldImageHash(acmObj.getAdImageHash()).setFieldMessage("Ads").setFieldLink("https://www.xpressdocs.com"))).execute();
			return creative.toString();

		} catch (Exception e) {
			System.err.println(e.getMessage());
			return e.getMessage();
		}

	}
	

}
