package com.xpressdocs.Model;

import com.facebook.ads.sdk.APIException;
import com.facebook.ads.sdk.AdCreative;
import com.xpressdocs.Utils.FacebookConfig;

public class AdModel extends FacebookConfig {
	private String adName;
	private String adSetId;
	private String adCreativeId;
	private String adStatus;
	private String adBidAmount;
	private String adReDownload;

	public String getAdName() {
		return adName;
	}

	public void setAdName(String adName) {
		this.adName = adName;
	}

	public String getAdSetId() {
		return adSetId;
	}

	public void setAdSetId(String adSetId) {
		this.adSetId = adSetId;
	}

	public String getAdCreativeId() {
		return adCreativeId;
	}

	public void setAdCreativeId(String adCreativeId) {
		this.adCreativeId = adCreativeId;
	}

	public String getAdStatus() {
		return adStatus;
	}

	public void setAdStatus(String adStatus) {
		this.adStatus = adStatus;
	}

	public String getAdBidAmount() {
		return adBidAmount;
	}

	public void setAdBidAmount(String adBidAmount) {
		this.adBidAmount = adBidAmount;
	}

	public String getAdReDownload() {
		return adReDownload;
	}

	public void setAdReDownload(String adReDownload) {
		this.adReDownload = adReDownload;
	}

	

}
