package com.xpressdocs.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.security.MD5Encoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.facebook.ads.sdk.AdImage;
import com.xpressdocs.Utils.FacebookConfig;

@Service
public class AdImageServiceImpl extends FacebookConfig implements AdImageService {

	@Override
	public String uploadImage(String file) {
		buildConfig();
		byte[] b = javax.xml.bind.DatatypeConverter.parseBase64Binary(file);
		String fileName = new Date().getTime()+".png";
		String path = "/Users/rabdul/Desktop/ServerImages/" + fileName;
		AdImage image = null;
		File imageFile = new File(path);
		try {
			FileUtils.writeByteArrayToFile(imageFile, b);
			image = getAccount().createAdImage().addUploadFile(fileName, imageFile).execute();
			
			return image.toString();
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return e.toString();
		}
		
		
	}

//	@Override
//	public AdImage uploadImage(MultipartFile file) {
//		String fileName = file.getOriginalFilename();
//		String path = "/Users/rabdul/Desktop/" + fileName;
//		byte[] buffer = new byte[1000];
//		File outputFile = new File(path);
//		InputStream reader = null;
//		FileOutputStream writer = null;
//		int totalBytes = 0;
//		AdImage image = null;
//		
//		buildConfig();
//
//		try {
//			outputFile.createNewFile();
//
//			// Create the input stream to uploaded file to read data from it.
//			reader = (InputStream) file.getInputStream();
//
//			// Create writer for 'outputFile' to write data read from
//			// 'uploadedFileRef'
//			writer = new FileOutputStream(outputFile);
//
//			// Iteratively read data from 'uploadedFileRef' and write to
//			// 'outputFile';
//			int bytesRead = 0;
//			while ((bytesRead = reader.read(buffer)) != -1) {
//				writer.write(buffer);
//				totalBytes += bytesRead;
//			}
//
//			image = getAccount().createAdImage().addUploadFile(file.getOriginalFilename(), new File(path)).execute();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				reader.close();
//				writer.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//
//		return image;
//	}

}
