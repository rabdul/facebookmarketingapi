package com.xpressdocs.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.facebook.ads.sdk.AdSet;
import com.xpressdocs.Model.AdSetModel;
import com.xpressdocs.Service.AdSetService;

@RestController
@RequestMapping("/adset")
public class AdSetControllers {

	@Autowired
	AdSetService adsetService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createAdSet(@RequestBody AdSetModel obj) {
		
		 
		return adsetService.createAdSet(obj);

	}

}
