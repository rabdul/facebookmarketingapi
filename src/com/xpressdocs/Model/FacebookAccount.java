package com.xpressdocs.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fb_ad_accounts")
public class FacebookAccount {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int ID;
	@Column(name="company_id")
	private int companyID;
	
	@Column(name="access_token")
	private String accessToken;
	
	@Column(name="account_id")
	private String accountID;
	
	@Column(name="app_secert")
	private String appSecert;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getCompanyID() {
		return companyID;
	}

	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getAppSecert() {
		return appSecert;
	}

	public void setAppSecert(String appSecert) {
		this.appSecert = appSecert;
	}


}
