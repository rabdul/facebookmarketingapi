package com.xpressdocs.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xpressdocs.Model.AdCreativeModel;
import com.xpressdocs.Service.AdCreativeService;

@RestController
@RequestMapping("/adcreative")
public class AdCreativeController {

	@Autowired
	AdCreativeService acs;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createAdCreative(@RequestBody AdCreativeModel adCreativeObj) {
		return acs.createAdCreative(adCreativeObj);
	}
}
