package com.xpressdocs.Service;

import org.springframework.stereotype.Service;
import com.facebook.ads.sdk.Campaign;
import com.xpressdocs.Model.CampaignModel;
import com.xpressdocs.Utils.FacebookConfig;

@Service
public class CampaignServiceImpl extends FacebookConfig implements CampaignService {

	@Override
	public Campaign createCampaign(CampaignModel obj) {
		Campaign campaign = null;
		buildConfig();
		
		try {
			campaign = getAccount().createCampaign().setName(obj.getCampaignName())
					.setObjective(obj.getCampaignObjective()).setSpendCap(obj.getCampaignSpendCap())
					.setStatus(obj.getCampaignStatus()).execute();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.out.println(e.getMessage());
		}

		return campaign;

	}

}
