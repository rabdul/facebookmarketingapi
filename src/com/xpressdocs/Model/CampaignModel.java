package com.xpressdocs.Model;

import com.facebook.ads.sdk.AdPreview;
import com.facebook.ads.sdk.Campaign;

public class CampaignModel {

	private String campaignName;
	private String campaignStatus;
	private String campaignObjective;
	private String campaignSpendCap;

	public Long getCampaignSpendCap() {
		return Long.parseLong(campaignSpendCap);
	}

	public void setCampaignSpendCap(String campaignSpendCap) {
		this.campaignSpendCap = campaignSpendCap;
	}

	public Campaign.EnumObjective getCampaignObjective() {
		return getCampaignObjective(campaignObjective);
	}

	public void setCampaignObjective(String campaignObjective) {
		this.campaignObjective = campaignObjective;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public Campaign.EnumStatus getCampaignStatus() {
		return getCampaignStatus(campaignStatus);
	}

	public void setCampaignStatus(String campaignStatus) {
		this.campaignStatus = campaignStatus;
	}

	private Campaign.EnumObjective getCampaignObjective(String campaignObjective) {

		switch (campaignObjective.toLowerCase()) {
		case "null":
			return Campaign.EnumObjective.NULL;
		case "app_installs":
			return Campaign.EnumObjective.VALUE_APP_INSTALLS;
		case "brand_awareness":
			return Campaign.EnumObjective.VALUE_BRAND_AWARENESS;
		case "conversions":
			return Campaign.EnumObjective.VALUE_CONVERSIONS;
		case "event_responses":
			return Campaign.EnumObjective.VALUE_EVENT_RESPONSES;
		case "lead_generation":
			return Campaign.EnumObjective.VALUE_LEAD_GENERATION;
		case "link_clicks":
			return Campaign.EnumObjective.VALUE_LINK_CLICKS;
		case "local_awareness":
			return Campaign.EnumObjective.VALUE_LOCAL_AWARENESS;
		case "offer_claims":
			return Campaign.EnumObjective.VALUE_OFFER_CLAIMS;
		case "page_likes":
			return Campaign.EnumObjective.VALUE_PAGE_LIKES;
		case "post_engagement":
			return Campaign.EnumObjective.VALUE_POST_ENGAGEMENT;
		case "product_catalog_sales":
			return Campaign.EnumObjective.VALUE_PRODUCT_CATALOG_SALES;
		case "reach":
			return Campaign.EnumObjective.VALUE_REACH;
		case "video_views":
			return Campaign.EnumObjective.VALUE_VIDEO_VIEWS;

		default:
			return Campaign.EnumObjective.NULL;
		}

	}

	private Campaign.EnumStatus getCampaignStatus(String campaignStatus) {

		switch (campaignStatus.toLowerCase()) {
		case "null":
			return Campaign.EnumStatus.NULL;
		case "active":
			return Campaign.EnumStatus.VALUE_ACTIVE;
		case "archived":
			return Campaign.EnumStatus.VALUE_ARCHIVED;
		case "deleted":
			return Campaign.EnumStatus.VALUE_DELETED;
		case "paused":
			return Campaign.EnumStatus.VALUE_PAUSED;

		default:
			return Campaign.EnumStatus.NULL;
		}
	}

}
