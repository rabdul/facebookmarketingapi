<?php

/**
 * Created by PhpStorm.
 * User: rabdul
 * Date: 7/24/17
 * Time: 4:54 PM
 */
class index
{
    private $serverUrl = "http://172.20.19.177:8080/FacebookRestApi/";
    private $previewUrl = "https://graph.facebook.com/v2.9/";
    private $campaignUrl = "campaign/create";
    private $adSetUrl = "adset/create";
    private $adImageUrl = "adimage/upload";
    private $adCreativeUrl = "adcreative/create";
    private $adUrl = "ad/create";
    private $method = "POST";
    private $previewMethod="/previews?access_token=EAAZAzs790EboBAHTl7UEw0FexvgdOszEsdCQ4hRUSGwNhxHg8K25nR7KIvDiWbd1gEOgIaleEI2JgZCfT8BQoxTZAS1mTz2qzCm33jcTjvqCl5qTZBnekmDhGK3pdqihyJ1bqffE6UVmlBnOxLb3jYl0qy7znEFtKSXdn5Uhieo5Gw8Btyb0h7HrltATXeRUYATls3wxNLTK2QNGGofq&ad_format=RIGHT_COLUMN_STANDARD";
    private $reqMethod = "";

    public function __construct()
    {
        $this->reqMethod = $_SERVER['REQUEST_METHOD'];
        $this->body();

    }


    public function body()
    {
        try {
            if($this->reqMethod == 'POST') {
//                $inputJSON = file_get_contents('php://input');
//                $request_array = json_decode($inputJSON, true);
                $adPreviewFrame = $this->buildAd($_POST,$_FILES);

                echo $adPreviewFrame;
            }
            else{
                echo $this->buildBody();
            }

        }
        catch (Exception $e){

        }


    }

    private function buildBody(){
        $body = "<form action='index.php' method='post' enctype='multipart/form-data'>
                    Campaign Name : <input type='text' name='campaignName' value='Rest API XD_campaign'/><br>
                    Campaign Status : <input type='text' name='campaignStatus' value='paused'/><br>
                    Campaign Objective : <input type='text' name='campaignObjective' value='link_clicks'/><br>
                    Campaign SpendCap : <input type='text' name='campaignSpendCap' value='10000'/><br>
                    AdSet Name : <input type='text' name='adSetName' value='Rest API XD_adset'/><br>
                    AdSet Status : <input type='text' name='adSetStatus' value='active'/><br>
                    Ad Billing Event : <input type='text' name='adBillingEvent' value='impressions'/><br>
                    Ad Daily Budget : <input type='text' name='adDailyBudget' value='500'/><br>
                    Ad Bid Amount : <input type='text' name='adBidAmount' value='100'/><br>
                    Ad Optimization Goals : <input type='text' name='adOptimizationGoal' value='impressions'/><br>
                    Ad Targeting : <input type='text' name='adTargeting' value='10000'/><br>
                    Ad Re-Download : <input type='text' name='adRedownload' value='true'/><br>
                    Ad Creative Name : <input type='text' name='adCreativeName' value='Want a business card?'/><br>
                    Ad Creative Body : <input type='text' name='adCreativeBody' value='Its costs just a dollar'/><br>
                    Ad Creative Link URL : <input type='text' name='adCreativeLinkUrl' value='www.xpressdocs.com'/><br>
                    Ad Creative Object URL : <input type='text' name='adCreativeObjectUrl' value='www.xpressdocs.com'/><br>
                    Ad Name : <input type='text' name='adName' value='Rest API XD-ad'/><br>
                    Ad Status : <input type='text' name='adStatus' value='ACTIVE'/><br>
                    Ad Re-Download : <input type='text' name='adReDownload' value='true'/><br>
                    Image : <input type='file' name='uploadFile' /></br>
                    <input type='submit' value='Create Ad'>
              
                </form>";

        return $body;
    }

    private function buildAd($request_array, $files){
        $campaignId = $this->createCampaign($request_array);
        $adSetId = $this->createAdSet($request_array,$campaignId);
        $imageHash = $this->uploadImage($files);
        $adCreativeId = $this->createAdCreative($request_array,$imageHash);
        $adId = $this->createAd($request_array,$adSetId,$adCreativeId);
        $adPreviewFrame = $this->createAdPreview($adId);

        return $adPreviewFrame;
    }

    private function createCampaign($requestData){
        $campaignData['campaignName'] = $requestData['campaignName'];
        $campaignData['campaignStatus'] = $requestData['campaignStatus'] ;
        $campaignData['campaignObjective'] = $requestData['campaignObjective'];
        $campaignData['campaignSpendCap'] = $requestData['campaignSpendCap'];

        $campagainRes = json_decode($this->CallAPI($this->method,$this->serverUrl.$this->campaignUrl,json_encode($requestData)),true);

        return $campagainRes['id'];
    }

    private function createAdSet($requestData,$campaignId){
        $adSetData['adSetName'] = $requestData['adSetName'];
        $adSetData['campaignId'] = $campaignId ;
        $adSetData['adSetStatus'] = $requestData['adSetStatus'];
        $adSetData['adBillingEvent'] = $requestData['adBillingEvent'];
        $adSetData['adDailyBudget'] = $requestData['adDailyBudget'];
        $adSetData['adBidAmount'] = $requestData['adBidAmount'] ;
        $adSetData['adOptimizationGoal'] = $requestData['adOptimizationGoal'];
        $adSetData['adTargeting'] = $requestData['adTargeting'];
        $adSetData['adRedownload'] = $requestData['adRedownload'];

        $adSetRes = json_decode($this->CallAPI($this->method,$this->serverUrl.$this->adSetUrl,json_encode($adSetData)),true);

        return $adSetRes['id'];
    }

    private function uploadImage($requestData){

        $uploadImagedata['uploadFile'] = base64_encode(file_get_contents($requestData['uploadFile']['tmp_name']));

        $imageUploadRes = json_decode($this->CallAPI($this->method,$this->serverUrl.$this->adImageUrl,json_encode($uploadImagedata)),true);

        return $imageUploadRes['hash'];

    }

    private function createAdCreative($requestData,$imageHash){
        $adCreativeData['adCreativeName'] = $requestData['adCreativeName'];
        $adCreativeData['adCreativeBody'] = $requestData['adCreativeBody'] ;
        $adCreativeData['adImageHash'] = $imageHash;
        $adCreativeData['adCreativeLinkUrl'] = $requestData['adCreativeLinkUrl'];
        $adCreativeData['adCreativeObjectUrl'] = $requestData['adCreativeObjectUrl'];

        $adCreativeRes = json_decode($this->CallAPI($this->method,$this->serverUrl.$this->adCreativeUrl,json_encode($adCreativeData)),true);

        return $adCreativeRes['creative_id'];


    }

    private function createAd($requestData,$adSetId,$adCreativeId){
        $adData['adName'] = $requestData['adName'];
        $adData['adSetId'] = $adSetId ;
        $adData['adCreativeId'] = $adCreativeId;
        $adData['adStatus'] = $requestData['adStatus'];
        $adData['adBidAmount'] = $requestData['adBidAmount'];
        $adData['adReDownload'] = $requestData['adReDownload'];

        $adRes = json_decode($this->CallAPI($this->method,$this->serverUrl.$this->adUrl,json_encode($adData)),true);

        return $adRes['id'];
    }

    private function createAdPreview($adId){

        $adPreviewRes = json_decode($this->CallAPI("GET",$this->previewUrl.$adId.$this->previewMethod),true);

        return $adPreviewRes["data"][0]["body"];

    }



    private function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            'Accept:application/json'
        ));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

}

new index();