package com.xpressdocs.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.facebook.ads.sdk.Campaign;
import com.xpressdocs.Model.CampaignModel;
import com.xpressdocs.Service.CampaignService;

@RestController
@RequestMapping("/campaign")
public class CampaignControllers {

	@Autowired
	CampaignService campaignService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<String> createCampaign(@RequestBody CampaignModel obj) {
		Campaign resultCampaign = null;
		resultCampaign = campaignService.createCampaign(obj);
		return ResponseEntity.status(200).body(resultCampaign.toString());

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateCampaign(@RequestBody CampaignModel obj) {
		Campaign resultCampaign = null;
		resultCampaign = campaignService.createCampaign(obj);
		return resultCampaign.toString();

	}

}
