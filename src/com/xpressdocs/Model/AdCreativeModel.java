package com.xpressdocs.Model;

public class AdCreativeModel {
	
	private String adCreativeName;
	private String adCreativeBody;
	private String adImageHash;
	private String adCreativeLinkUrl;
	private String adCreativeObjectUrl;
	
	public String getAdCreativeName() {
		return adCreativeName;
	}
	public void setAdCreativeName(String adCreativeName) {
		this.adCreativeName = adCreativeName;
	}
	public String getAdImageHash() {
		return adImageHash;
	}
	public void setAdImageHash(String adImageHash) {
		this.adImageHash = adImageHash;
	}
	public String getAdCreativeLinkUrl() {
		return adCreativeLinkUrl;
	}
	public void setAdCreativeLinkUrl(String adCreativeLinkUrl) {
		this.adCreativeLinkUrl = adCreativeLinkUrl;
	}
	public String getAdCreativeObjectUrl() {
		return adCreativeObjectUrl;
	}
	public void setAdCreativeObjectUrl(String adCreativeObjectUrl) {
		this.adCreativeObjectUrl = adCreativeObjectUrl;
	}
	public String getAdCreativeBody() {
		return adCreativeBody;
	}
	public void setAdCreativeBody(String adCreativeBody) {
		this.adCreativeBody = adCreativeBody;
	}
	
	

}
